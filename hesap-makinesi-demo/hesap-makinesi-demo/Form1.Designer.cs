﻿namespace hesap_makinesi_demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.girilenDeger = new System.Windows.Forms.TextBox();
            this.sonucGoster = new System.Windows.Forms.Label();
            this.btnSin = new System.Windows.Forms.Button();
            this.basilanTuslar = new System.Windows.Forms.Label();
            this.btnCos = new System.Windows.Forms.Button();
            this.btnTan = new System.Windows.Forms.Button();
            this.btnCE = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.btnExp = new System.Windows.Forms.Button();
            this.btnLog = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnParantezSag = new System.Windows.Forms.Button();
            this.btnVirgül = new System.Windows.Forms.Button();
            this.btnParantezSol = new System.Windows.Forms.Button();
            this.btnPi = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.btnKare = new System.Windows.Forms.Button();
            this.btnFaktoriyel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(79, 356);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 55);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.Location = new System.Drawing.Point(261, 453);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(64, 55);
            this.button7.TabIndex = 6;
            this.button7.Text = "+";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button8.Location = new System.Drawing.Point(199, 356);
            this.button8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(64, 55);
            this.button8.TabIndex = 7;
            this.button8.Text = "3";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button9.Location = new System.Drawing.Point(139, 356);
            this.button9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(64, 55);
            this.button9.TabIndex = 8;
            this.button9.Text = "2";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button10.Location = new System.Drawing.Point(139, 404);
            this.button10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(64, 55);
            this.button10.TabIndex = 12;
            this.button10.Text = "5";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button11.Location = new System.Drawing.Point(199, 404);
            this.button11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(64, 55);
            this.button11.TabIndex = 11;
            this.button11.Text = "6";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button12.Location = new System.Drawing.Point(261, 404);
            this.button12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(64, 55);
            this.button12.TabIndex = 10;
            this.button12.Text = "-";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button13.Location = new System.Drawing.Point(79, 404);
            this.button13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(64, 55);
            this.button13.TabIndex = 9;
            this.button13.Text = "4";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button14.Location = new System.Drawing.Point(139, 453);
            this.button14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(64, 55);
            this.button14.TabIndex = 16;
            this.button14.Text = "8";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button15.Location = new System.Drawing.Point(199, 453);
            this.button15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(64, 55);
            this.button15.TabIndex = 15;
            this.button15.Text = "9";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button16.Location = new System.Drawing.Point(261, 356);
            this.button16.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(64, 55);
            this.button16.TabIndex = 14;
            this.button16.Text = "*";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button17.Location = new System.Drawing.Point(79, 453);
            this.button17.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(64, 55);
            this.button17.TabIndex = 13;
            this.button17.Text = "7";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button2.Location = new System.Drawing.Point(139, 502);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 55);
            this.button2.TabIndex = 17;
            this.button2.Text = "0";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Location = new System.Drawing.Point(261, 502);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(64, 55);
            this.button4.TabIndex = 19;
            this.button4.Text = "=";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.Location = new System.Drawing.Point(261, 305);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(64, 55);
            this.button5.TabIndex = 20;
            this.button5.Text = "÷";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // girilenDeger
            // 
            this.girilenDeger.Location = new System.Drawing.Point(16, 90);
            this.girilenDeger.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.girilenDeger.Name = "girilenDeger";
            this.girilenDeger.Size = new System.Drawing.Size(308, 22);
            this.girilenDeger.TabIndex = 21;
            this.girilenDeger.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // sonucGoster
            // 
            this.sonucGoster.AutoSize = true;
            this.sonucGoster.Location = new System.Drawing.Point(216, 46);
            this.sonucGoster.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.sonucGoster.Name = "sonucGoster";
            this.sonucGoster.Size = new System.Drawing.Size(0, 16);
            this.sonucGoster.TabIndex = 22;
            this.sonucGoster.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.sonucGoster.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnSin
            // 
            this.btnSin.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSin.Location = new System.Drawing.Point(139, 207);
            this.btnSin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSin.Name = "btnSin";
            this.btnSin.Size = new System.Drawing.Size(64, 55);
            this.btnSin.TabIndex = 23;
            this.btnSin.Text = "sin";
            this.btnSin.UseVisualStyleBackColor = true;
            this.btnSin.Click += new System.EventHandler(this.btnSin_Click);
            // 
            // basilanTuslar
            // 
            this.basilanTuslar.AutoSize = true;
            this.basilanTuslar.Location = new System.Drawing.Point(321, 11);
            this.basilanTuslar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.basilanTuslar.Name = "basilanTuslar";
            this.basilanTuslar.Size = new System.Drawing.Size(0, 16);
            this.basilanTuslar.TabIndex = 24;
            this.basilanTuslar.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnCos
            // 
            this.btnCos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCos.Location = new System.Drawing.Point(199, 207);
            this.btnCos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCos.Name = "btnCos";
            this.btnCos.Size = new System.Drawing.Size(64, 55);
            this.btnCos.TabIndex = 25;
            this.btnCos.Text = "cos";
            this.btnCos.UseVisualStyleBackColor = true;
            this.btnCos.Click += new System.EventHandler(this.btnCos_Click);
            // 
            // btnTan
            // 
            this.btnTan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTan.Location = new System.Drawing.Point(261, 207);
            this.btnTan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTan.Name = "btnTan";
            this.btnTan.Size = new System.Drawing.Size(64, 55);
            this.btnTan.TabIndex = 26;
            this.btnTan.Text = "tan";
            this.btnTan.UseVisualStyleBackColor = true;
            this.btnTan.Click += new System.EventHandler(this.btnTan_Click);
            // 
            // btnCE
            // 
            this.btnCE.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCE.Location = new System.Drawing.Point(79, 305);
            this.btnCE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCE.Name = "btnCE";
            this.btnCE.Size = new System.Drawing.Size(64, 55);
            this.btnCE.TabIndex = 27;
            this.btnCE.Text = "CE";
            this.btnCE.UseVisualStyleBackColor = true;
            this.btnCE.Click += new System.EventHandler(this.btnCE_Click);
            // 
            // btnC
            // 
            this.btnC.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnC.Location = new System.Drawing.Point(139, 305);
            this.btnC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(64, 55);
            this.btnC.TabIndex = 28;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // button18
            // 
            this.button18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button18.Location = new System.Drawing.Point(199, 305);
            this.button18.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(64, 55);
            this.button18.TabIndex = 29;
            this.button18.Text = "silme";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnExp
            // 
            this.btnExp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExp.Location = new System.Drawing.Point(199, 256);
            this.btnExp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExp.Name = "btnExp";
            this.btnExp.Size = new System.Drawing.Size(64, 55);
            this.btnExp.TabIndex = 30;
            this.btnExp.Text = "Exp";
            this.btnExp.UseVisualStyleBackColor = true;
            this.btnExp.Click += new System.EventHandler(this.btnExp_Click);
            // 
            // btnLog
            // 
            this.btnLog.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLog.Location = new System.Drawing.Point(139, 256);
            this.btnLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(64, 55);
            this.btnLog.TabIndex = 31;
            this.btnLog.Text = "log";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // button19
            // 
            this.button19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button19.Location = new System.Drawing.Point(79, 256);
            this.button19.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(64, 55);
            this.button19.TabIndex = 32;
            this.button19.Text = "10^x";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button20
            // 
            this.button20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button20.Location = new System.Drawing.Point(261, 256);
            this.button20.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(64, 55);
            this.button20.TabIndex = 33;
            this.button20.Text = "Mod";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(79, 207);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 55);
            this.button3.TabIndex = 34;
            this.button3.Text = "x^y";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnParantezSag
            // 
            this.btnParantezSag.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnParantezSag.Location = new System.Drawing.Point(79, 502);
            this.btnParantezSag.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnParantezSag.Name = "btnParantezSag";
            this.btnParantezSag.Size = new System.Drawing.Size(64, 55);
            this.btnParantezSag.TabIndex = 35;
            this.btnParantezSag.Text = ")";
            this.btnParantezSag.UseVisualStyleBackColor = true;
            // 
            // btnVirgül
            // 
            this.btnVirgül.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnVirgül.Location = new System.Drawing.Point(199, 502);
            this.btnVirgül.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnVirgül.Name = "btnVirgül";
            this.btnVirgül.Size = new System.Drawing.Size(64, 55);
            this.btnVirgül.TabIndex = 36;
            this.btnVirgül.Text = ",";
            this.btnVirgül.UseVisualStyleBackColor = true;
            this.btnVirgül.Click += new System.EventHandler(this.btnVirgül_Click);
            // 
            // btnParantezSol
            // 
            this.btnParantezSol.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnParantezSol.Location = new System.Drawing.Point(16, 502);
            this.btnParantezSol.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnParantezSol.Name = "btnParantezSol";
            this.btnParantezSol.Size = new System.Drawing.Size(64, 55);
            this.btnParantezSol.TabIndex = 37;
            this.btnParantezSol.Text = "(";
            this.btnParantezSol.UseVisualStyleBackColor = true;
            // 
            // btnPi
            // 
            this.btnPi.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnPi.Location = new System.Drawing.Point(16, 356);
            this.btnPi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPi.Name = "btnPi";
            this.btnPi.Size = new System.Drawing.Size(64, 55);
            this.btnPi.TabIndex = 38;
            this.btnPi.Text = " π";
            this.btnPi.UseVisualStyleBackColor = true;
            this.btnPi.Click += new System.EventHandler(this.btnPi_Click);
            // 
            // button21
            // 
            this.button21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button21.Location = new System.Drawing.Point(16, 256);
            this.button21.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(64, 55);
            this.button21.TabIndex = 39;
            this.button21.Text = "√";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button22
            // 
            this.button22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button22.Location = new System.Drawing.Point(16, 453);
            this.button22.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(64, 55);
            this.button22.TabIndex = 40;
            this.button22.Text = "+-";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // btnKare
            // 
            this.btnKare.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnKare.Location = new System.Drawing.Point(16, 207);
            this.btnKare.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnKare.Name = "btnKare";
            this.btnKare.Size = new System.Drawing.Size(64, 55);
            this.btnKare.TabIndex = 41;
            this.btnKare.Text = "x^2";
            this.btnKare.UseVisualStyleBackColor = true;
            this.btnKare.Click += new System.EventHandler(this.btnKare_Click);
            // 
            // btnFaktoriyel
            // 
            this.btnFaktoriyel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnFaktoriyel.Location = new System.Drawing.Point(16, 404);
            this.btnFaktoriyel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFaktoriyel.Name = "btnFaktoriyel";
            this.btnFaktoriyel.Size = new System.Drawing.Size(64, 55);
            this.btnFaktoriyel.TabIndex = 42;
            this.btnFaktoriyel.Text = "n!";
            this.btnFaktoriyel.UseVisualStyleBackColor = true;
            this.btnFaktoriyel.Click += new System.EventHandler(this.btnFaktoriyel_Click);
            // 
            // btnOk
            // 
            this.btnOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnOk.Location = new System.Drawing.Point(16, 305);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(64, 55);
            this.btnOk.TabIndex = 43;
            this.btnOk.Text = "ok";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button6.Location = new System.Drawing.Point(165, 158);
            this.button6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(60, 55);
            this.button6.TabIndex = 44;
            this.button6.Text = "x^2";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button23
            // 
            this.button23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button23.Location = new System.Drawing.Point(117, 158);
            this.button23.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(49, 55);
            this.button23.TabIndex = 45;
            this.button23.Text = "x^2";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button24
            // 
            this.button24.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button24.Location = new System.Drawing.Point(16, 158);
            this.button24.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(55, 55);
            this.button24.TabIndex = 46;
            this.button24.Text = "x^2";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button25.Location = new System.Drawing.Point(67, 158);
            this.button25.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(52, 55);
            this.button25.TabIndex = 47;
            this.button25.Text = "x^2";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button26.Location = new System.Drawing.Point(271, 158);
            this.button26.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(55, 55);
            this.button26.TabIndex = 48;
            this.button26.Text = "x^2";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button27.Location = new System.Drawing.Point(221, 158);
            this.button27.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(53, 55);
            this.button27.TabIndex = 49;
            this.button27.Text = "x^2";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(960, 618);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnFaktoriyel);
            this.Controls.Add(this.btnKare);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.btnPi);
            this.Controls.Add(this.btnParantezSol);
            this.Controls.Add(this.btnVirgül);
            this.Controls.Add(this.btnParantezSag);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.btnExp);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnCE);
            this.Controls.Add(this.btnTan);
            this.Controls.Add(this.btnCos);
            this.Controls.Add(this.basilanTuslar);
            this.Controls.Add(this.btnSin);
            this.Controls.Add(this.sonucGoster);
            this.Controls.Add(this.girilenDeger);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox girilenDeger;
        private System.Windows.Forms.Label sonucGoster;
        private System.Windows.Forms.Button btnSin;
        private System.Windows.Forms.Label basilanTuslar;
        private System.Windows.Forms.Button btnCos;
        private System.Windows.Forms.Button btnTan;
        private System.Windows.Forms.Button btnCE;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnExp;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnParantezSag;
        private System.Windows.Forms.Button btnVirgül;
        private System.Windows.Forms.Button btnParantezSol;
        private System.Windows.Forms.Button btnPi;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button btnKare;
        private System.Windows.Forms.Button btnFaktoriyel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
    }
}

