﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace hesap_makinesi_demo
{
    public partial class Form1 : Form
    {
        int geciciDeger=0;               //ustel fonksiyonunda kullanılacak olan değişkeni tanımladık.
        double sonuc = 0;                //değişkenleri tanımlıyoruz.
        string op;

        public Form1()
        {
            InitializeComponent();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            degerGir("3");  //3 basıldığında degerGir fonk. çağrılacak.
        }

        private void button14_Click(object sender, EventArgs e)
        {
            degerGir("8");   //8 basıldığında degerGir fonk. çağrılacak.
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            degerGir("1"); //1 basıldığında degerGir fonk. çağrılacak.
        }


        private void button9_Click(object sender, EventArgs e)
        {
            degerGir("2"); //2 basıldığında degerGir fonk. çağrılacak.
        }

        private void button13_Click(object sender, EventArgs e)
        {
            degerGir("4");  //4 basıldığında degerGir fonk. çağrılacak.
        }

        private void button10_Click(object sender, EventArgs e)
        {
            degerGir("5");  //5 basıldığında degerGir fonk. çağrılacak.
        }

        private void button11_Click(object sender, EventArgs e)
        {
            degerGir("6");  //6 basıldığında degerGir fonk. çağrılacak.
        }

        private void button17_Click(object sender, EventArgs e)
        {
            degerGir("7");  //7 basıldığında degerGir fonk. çağrılacak.
        }

        private void button15_Click(object sender, EventArgs e)
        {
            degerGir("9");  //9 basıldığında degerGir fonk. çağrılacak.
        }

        private void button2_Click(object sender, EventArgs e)
        {
            degerGir("0");  // 0 basıldığında degerGir fonk. çağrılacak.
        }


        private void ustel()  //ustel fonksiyonunu tanımlıyoruz .
        {
            //ustelHesabi değişkeni tanımlıyoruz ve bu değişkene pow fonksiyonu ile değer atıyoruz
            double ustelHesabi = Math.Pow(Double.Parse(geciciDeger.ToString()), Double.Parse(girilenDeger.Text));
            sonucGoster.Text = ustelHesabi.ToString(); //sonucGoster in textini ustelHesabinın string değerine eşitliyoruz.
            girilenDeger.Text = ""; //Bu işlemler yapıldıktan sonra girilenDeger textini sıfırlıyoruz.
            geciciDeger = 0; //geciciDeğer değişkenine sıfırı atıyoruz.
        }

        private void onUssu() //On üzeri x fonksiyonunu tanımlıyoruz.
        {
            ////usHesabi değişkeni tanımlıyoruz ve bu değişkene pow fonksiyonu ile değer atıyoruz
            double usHesabi = Math.Pow(10, Double.Parse(girilenDeger.Text));
            sonucGoster.Text = usHesabi.ToString(); //sonucGoster in textine usHesabi dğişkenini string yapıp atıyoruz
            girilenDeger.Text = "";
        }

        private void kare() //x kare (x^2) fonksiyonunu tanımlıyoruz.
        {
            //kareHesabi değişkeni tanımlıyoruz ve bu değişkene pow fonksiyonu ile değer atıyoruz
            double kareHesabi = Math.Pow(Double.Parse(girilenDeger.Text), 2);
            sonucGoster.Text = kareHesabi.ToString();  //sonucGoster in textine kareHesabı değişkenini string yapıp atıyoruz.
            girilenDeger.Text = ""; //Bu işlemlerden sonra girilenDeger textini sıfırlıyoruz.
        }
        private void kareKok() //kareKok fonksiyonu tanımlıyoruz.
        {
            //kokHesabi değişkeni tanımlıyoruz ve bu değişkene Sqrt fonksiyonu ile değer atıyoruz
            double kokHesabi = Math.Sqrt(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = kokHesabi.ToString(); //sonucGoster in textine kokHesabi değişkenini string yapıp atıyoruz.
            girilenDeger.Text = "";// Bu işlemlerden sonra textin içini sıfırlıyoruz.
        }

        private void faktoriyel()  //Faktoriyel fonk. tanımlıyoruz.
        {
            // int tipinde sayi değişkeni tanımlıyoruz ve girilen değeri convert edip sayi değişkenine atıyoruz.
            int sayi = Convert.ToInt32(girilenDeger.Text);

            int hesapla = 1; // Hesaplamamıza yardımcı olacak değişken
            for (int i = 1; i <= sayi; i++) //  i<=sayi (hesaplanacak kadar sayı kadar dönecek)
            {
                hesapla = hesapla * i; // Hesaplama işlemimiz.
            }

            sonucGoster.Text = hesapla.ToString();  //sonucGoster in textine hesapla değişkenini stringe çevirip atıyoruz.
            girilenDeger.Text = "";  //Bu işlemlerden sonra sirilenDeger textini sıfırlıyoruz.
        }

        private void exp()   //Exp fonksiyonu tanımlıyoruz.
        {
            ////expHesabi değişkeni tanımlıyoruz ve bu değişkene Exp fonksiyonu ile değer atıyoruz
            double expHesabi = Math.Exp(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = expHesabi.ToString(); //sonucGoster değişkenine expHesabini stringe çevirip atıyoruz.
            girilenDeger.Text = "";//İşlemlerden sonra textin içini sifirliyoruz.
        }

        private void pi()//pi fonksiyonu tanımlıyoruz.
        {
            double piHesabi = Math.PI;//piHesabı değişkeni tanımlayıp PI fonksiyonu ile değer atıyoruz.
            sonucGoster.Text = (piHesabi * Double.Parse(girilenDeger.Text)).ToString();//sonucGoster textine işlemler yaptırıp sonucu stringe çevirip atıyoruz.
            girilenDeger.Text = "";// işlemlerden sonra textin içini sıfırlıyoruz. 
        }

        private void log() //logaritma fonksiyonunu tanımlıyoruz
        {
            //logHesabı değişkenitanımlıyoruz ve log fonksiyonu ile girilendeğerin sonucunu parse edip değişkene atıyoruz.
            double logHesabi = Math.Log(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = logHesabi.ToString();//sonucGosterin textine logHesabini stringe çevirip atıyoruz.
            girilenDeger.Text = ""; //son olarak textin içini sıfırlıyoruz.
        }

        private void mod() // mod fonksiyonu tanımlıyoruz.
        {
            //modHesabi değişkeni tanımlıyoruz ve bu değişkene Round fonksiyonu ile değer atıyoruz
            double modHesabi = Math.Round(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = modHesabi.ToString(); //modHesabi değerini stringe çevirip sonucGoster değişkenine atıyoruz.
            girilenDeger.Text = "";//son olarak textin içini sıfırlıyoruz.
        }

        private void cos()//Sırasıyla cos-sin ve tan fonksiyonları tanımlıyoruz.
        {
            double cosHesabi = Math.Cos(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = cosHesabi.ToString();
            girilenDeger.Text = "";
        }
        private void sin()
        {
            double sinHesabi = Math.Sin(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = sinHesabi.ToString();
            girilenDeger.Text = "";
        }

        private void tan()
        {
            double tanHesabi = Math.Tan(Double.Parse(girilenDeger.Text));
            sonucGoster.Text = tanHesabi.ToString();
            girilenDeger.Text = "";
        }

        private void hesapla1()//hesapla1 fonksiyonu tanımlıyoruz.
        {
            string strGirilenDeger = girilenDeger.Text;//string tipinde strGirilenDeger değişkeni tanımlıyoruz ve bu değişkene girilenDeğerin textini 

            if (strGirilenDeger == "")// Burada strGirilenDeger in sıfıra eşit olma durumu kontrol ediliyor
            {
                return;//koşul true dönerse herhangi bir işlem yapılmayacak
            }

            //op:operator
            switch (op)//Burada switch case yapısını açıyoruz.
            {
                case "+": //seçim artı ise aşağıdaki işlemleri yapacak.
                    sonuc = sonuc + Double.Parse(strGirilenDeger);// sonuc değişkenine sonuc un yeni değeri ile strgirilenDegeri parse edip ekliyoruz
                    sonucGoster.Text = sonuc.ToString(); //sonucGoster textine sonuc un yeni değerini stringe çevirip atıyoruz.
                    girilenDeger.Text = ""; //son olarak girilen texti sıfırlıyoruz
                    break;
                case "-": //seçim eksi ise aşağıdaki işlemleri yapacak.
                    sonuc = sonuc - Double.Parse(strGirilenDeger);//sonuc değişkenine sonuc un yeni değerinden strGirilenDeger i parse edip çıkarıyoruz.
                    sonucGoster.Text = sonuc.ToString();  //sonuc değişkenini string yapıp sonucGoster textine atıyoruz.
                    girilenDeger.Text = "";
                    break;
                case "*":  //seçim çarpma ise aşağıdaki işlemleri yapacak.
                    sonuc = sonuc * Double.Parse(strGirilenDeger);
                    sonucGoster.Text = sonuc.ToString();
                    girilenDeger.Text = "";
                    break;
                case "/":  //seçim bölme ise aşağıdaki işlemleri yapacak.
                    Double payda = Double.Parse(strGirilenDeger);
                    if (payda == 0)
                    {
                        sonucGoster.Text = "Sıfıra bölemezsiniz!";
                        break;

                    }
                    sonuc = sonuc / payda;
                    sonucGoster.Text = sonuc.ToString();
                    girilenDeger.Text = "";
                    break;

            }

        }

        private void degerGir(string deger) //degerGir fonksiyonu tanımlıyoruz ve parametre ise string türünde deger değişkeni .
        {
            girilenDeger.Text = girilenDeger.Text + deger;//girilenDeger textine girilenDegerin yeni değerini ve değer değikenini toplayarak atıyoruz.
            basilanTuslar.Text += deger; //basılanTuslar textine deger değişkenini toplattırıyoruz.
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            op = "+";
            hesapla1();
            basilanTuslar.Text += op;
            // operatör artı olduğunda hesapla fonksiyonunu çağiriyoruz.
        }

        private void button5_Click(object sender, EventArgs e)
        {
            op = "/";
            hesapla1();
            basilanTuslar.Text += op;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            op = "*";
            hesapla1();
            basilanTuslar.Text += op;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            op = "-";
            hesapla1();
            basilanTuslar.Text += op;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (geciciDeger!=0)
            {
                ustel();
                return;
            }


            basilanTuslar.Text += "=";
            hesapla1();
        }

        private void btnSin_Click(object sender, EventArgs e)
        {
            sin();
        }

        private void btnCos_Click(object sender, EventArgs e)
        {
            cos();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button27_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button23_Click(object sender, EventArgs e)
        {

        }

        private void btnTan_Click(object sender, EventArgs e)
        {
            tan();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            mod();
        }

        private void btnVirgül_Click(object sender, EventArgs e)
        {
            if ((girilenDeger.Text == "") | (girilenDeger.Text.EndsWith(",")))
            {
                girilenDeger.Text += "";
            }
            else
            {
                girilenDeger.Text += ",";
            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            log();
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            girilenDeger.Text = "";
            sonucGoster.Text = "";
            sonuc = 0;
        }

        private void btnCE_Click(object sender, EventArgs e)
        {
            sonucGoster.Text = "";
        }

        private void button18_Click(object sender, EventArgs e)
        {
            string geriSilme = girilenDeger.Text.Substring(0, (girilenDeger.Text.Length - 1));
            girilenDeger.Text = geriSilme;
        }

        private void btnPi_Click(object sender, EventArgs e)
        {
            pi();
        }

        private void btnFaktoriyel_Click(object sender, EventArgs e)
        {
            faktoriyel();
        }

        private void btnExp_Click(object sender, EventArgs e)
        {
            exp();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            kareKok();
        }

        private void btnKare_Click(object sender, EventArgs e)
        {
            kare();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            onUssu();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (geciciDeger==0)
            {
                geciciDeger = Convert.ToInt32(girilenDeger.Text);
                girilenDeger.Text = "";
            }
        }
    }
}
